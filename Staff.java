package phantomjsparser;

import java.util.HashSet;
import java.util.Set;

public class Staff {
	private Person person;
	private Profession profession;
	private Set<Role> roles;
	
	public Staff() {
		roles = new HashSet<>();
	}
	
	public Staff(Person person, Profession profession) {
		this.person = person;
		this.profession = profession;
	}
	
	public Staff(Person person, Profession profession, Set<Role> roles) {
		this.person = person;
		this.profession = profession;
		this.roles = roles;
	}

	public Set<Role> getRoles() {
		return roles;
	}
	
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public Profession getProfession() {
		return profession;
	}
	public void setProfession(Profession profession) {
		this.profession = profession;
	}
	
	public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Staff that = (Staff) o;

        if (person != null ? !person.equals(that.person) : that.person != null) return false;
        if (profession != null ? !profession.equals(that.profession) : that.profession != null) return false;
        if (roles != null ? !roles.equals(that.roles) : that.roles != null) return false;

        return true;
    }

    public int hashCode() {
    	int result;
        result = (person != null ? person.hashCode() : 0);
        result = 31 * result + (profession != null ? profession.hashCode() : 0);
        result = 31 * result + (roles != null ? roles.hashCode() : 0);
        return result;
    }
}
