package phantomjsparser.database;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.apache.commons.io.FileUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


public class QueryExecutor {
	private Connection con;
	private final static int DESCRIPTION_MAX_SIZE = 250;
	
	public void insertCinemas(JsonArray cinemas) throws SQLException {
		try {
			con = ConnectionDB.getConnection();
			for (JsonElement cinema : cinemas) {
				insertSingleCinema(cinema.getAsJsonObject());
			}
		} finally {
			con.close();
		}
	}
	
	public void insertTickets(JsonArray tickets) throws SQLException {
		PreparedStatement insertTicket = null;
		PreparedStatement selectCinemaId = null;
		PreparedStatement insertCinema = null;
		PreparedStatement selectFormatId = null;
		PreparedStatement insertFormat = null;
		try {
			con = ConnectionDB.getConnection();
			insertTicket = con.prepareStatement(Queries.INSERT_TICKET.getQuery());
			selectCinemaId = con.prepareStatement(Queries.SELECT_CINEMA_ID.getQuery());
			insertCinema = con.prepareStatement(Queries.INSERT_CINEMA_NAME.getQuery(), Statement.RETURN_GENERATED_KEYS);
			selectFormatId = con.prepareStatement(Queries.SELECT_FORMAT_ID.getQuery());
			insertFormat = con.prepareStatement(Queries.INSERT_FORMAT.getQuery(), Statement.RETURN_GENERATED_KEYS);
			
			for (JsonElement ticket : tickets) {
				insertTicket(ticket.getAsJsonObject(), selectCinemaId, insertCinema,
						insertTicket, selectFormatId, insertFormat);
			}
			
		} finally {
			if (insertFormat != null) insertFormat.close();
			if (selectFormatId != null) selectFormatId.close();
			if (insertCinema != null) insertCinema.close();
			if (selectCinemaId != null) selectCinemaId.close();
			if (insertTicket != null) insertTicket.close();
			if (con != null) con.close();
		}
	}
	
	public void insertCinema(JsonObject cinema) throws SQLException {
		try {
			con = ConnectionDB.getConnection();
			insertSingleCinema(cinema);
		} finally {
			con.close();
		}
	}
	
	public void deleteCinemas() throws SQLException {
		try {
			con = ConnectionDB.getConnection();
			Statement stmt = con.createStatement();
			stmt.execute(Queries.DELETE_CINEMAS.getQuery());
		} finally {
			con.close();
		}
	}
	
	public void deleteCinema(final String name) throws SQLException {
		PreparedStatement pr = null;
		try {
			con = ConnectionDB.getConnection();
			pr = con.prepareStatement(Queries.DELETE_CINEMA.getQuery());
			pr.setString(1, name);
			pr.executeUpdate();
		} finally {
			if (pr != null) pr.close();
			con.close();
		}
	}
	
	public void insertMovie(JsonObject movie) throws SQLException {
		PreparedStatement pr = null;
		try {
			con = ConnectionDB.getConnection();
			pr = con.prepareStatement(Queries.INSERT_MOVIE.getQuery());
			int id = movie.get("id").getAsInt();
			pr.setInt(1, id);
			pr.setString(2, movie.get("title").getAsString());
			
			// optional fields
			pr.setString(3, parseOptionalString(movie.get("original_title")));
			pr.setInt(4, parseOptionalInt(movie.get("production_year")));
			pr.setString(5, parseOptionalString(movie.get("tagline")));
			pr.setString(6, parseOptionalDate(movie.get("release_date")));
			pr.setInt(7, parseOptionalInt(movie.get("runtime")));
			pr.setString(8, parseOptionalDesc(movie.get("description")));
					
			pr.executeUpdate();
			pr.close();
			
			// parse countries
			insertToIntermediaryTable(movie.get("countries"), Queries.INSERT_COUNTRY, 
					Queries.INSERT_MOVIES_COUNTRIES, Queries.SELECT_COUNTRY_ID, id);
			
			// parse genres
			insertToIntermediaryTable(movie.get("genres"), Queries.INSERT_GENRE, 
					Queries.INSERT_MOVIES_GENRES, Queries.SELECT_GENRE_ID, id);
			
			// parse keywords
			insertToIntermediaryTable(movie.get("keywords"), Queries.INSERT_KEYWORD, 
					Queries.INSERT_MOVIES_KEYWORDS, Queries.SELECT_KEYWORD_ID, id);
			
			// parse IMDb
			insertIMDb(movie.get("IMDb"), id);
			
			// parse staff
			insertPersons(movie, id);
			
		} finally {
			if (pr != null) pr.close();
			if (con != null) con.close();
		}
	}
	
	private String parseOptionalString(JsonElement el) {
		if (el != null) return el.getAsString();
		return null;
	}
	
	private Integer parseOptionalInt(final JsonElement el) {
		if (el != null) return el.getAsInt();
		return null;
	}
	
	private String parseOptionalDate(JsonElement el) {
		if (el != null) {
			LocalDate date = LocalDate.parse(el.getAsString(), DateTimeFormatter.ofPattern("d MMMM yyyy", new Locale("ru")));
			return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		return null;
	}
	
	private String parseOptionalDesc(JsonElement el) {
		if (el != null) {
			String s = el.getAsString();
			if (s.length() > DESCRIPTION_MAX_SIZE) {
				s = s.substring(0, s.substring(0, DESCRIPTION_MAX_SIZE - 3).lastIndexOf(" ")) + "...";
			}
			return s;
		}
		return null;
	}
	
	private void insertSingleCinema(JsonObject cinema) throws SQLException {
		PreparedStatement pr = null;
		try {
			pr = con.prepareStatement(Queries.INSERT_CINEMA.getQuery());
			pr.setString(1, cinema.get("name").getAsString());
			pr.setString(2, cinema.get("address").getAsString());
			pr.executeUpdate();
		} finally {
			if (pr != null) pr.close();
		}
	}
	
	private void insertTicket(JsonObject ticket, PreparedStatement selectCinemaId, 
			PreparedStatement insertCinema, PreparedStatement insertTicket,
			PreparedStatement selectFormatId, PreparedStatement insertFormat) throws SQLException {
		
		insertTicket.setInt(1, getId(ticket.get("cinema"), selectCinemaId, insertCinema));
		insertTicket.setInt(2, ticket.get("movie_id").getAsInt());
		insertTicket.setInt(3, ticket.get("price").getAsInt());
		insertTicket.setString(4, ticket.get("time").getAsString());
		
		insertTicket.setInt(5, getId(ticket.get("format"), selectFormatId, insertFormat));
		insertTicket.executeUpdate();
		
		insertFormat.clearParameters();
		selectFormatId.clearParameters();
		insertCinema.clearParameters();
		selectCinemaId.clearParameters();
		insertTicket.clearParameters();
	}
	
	private void insertToIntermediaryTable(JsonElement arrayElement, Queries insert, Queries insertToIntermediaryTable, Queries select, int id) throws SQLException {
		if (arrayElement != null) {
			JsonArray array = arrayElement.getAsJsonArray();
			PreparedStatement insertStmt = null;
			PreparedStatement insertToInterStmt = null;
			PreparedStatement selectIdStmt = null;
			try {
				insertStmt = con.prepareStatement(insert.getQuery(), Statement.RETURN_GENERATED_KEYS);
				insertToInterStmt = con.prepareStatement(insertToIntermediaryTable.getQuery());
				selectIdStmt = con.prepareStatement(select.getQuery());
				
				for (JsonElement el : array) {
					
					insertToInterStmt.setInt(1, getId(el, selectIdStmt, insertStmt));
					insertToInterStmt.setInt(2, id);
					insertToInterStmt.executeUpdate();
					
					
					selectIdStmt.clearParameters();
					insertToInterStmt.clearParameters();
					insertStmt.clearParameters();
				}
			} finally {
				if (selectIdStmt != null) selectIdStmt.close();
				if (insertToInterStmt != null) insertToInterStmt.close();
				if (insertStmt != null) insertStmt.close();
			}
		}
	}
	
	private int getId(JsonElement el, PreparedStatement selectIdStmt, PreparedStatement insertStmt) throws SQLException {
		String name = el.getAsString();
		selectIdStmt.setString(1, name);
		ResultSet rs = null;
		int nameId = 0;

		try {
			rs = selectIdStmt.executeQuery();
			if (rs.next()) {
				nameId = rs.getInt(1);
			} else {
				insertStmt.setString(1, name);
				insertStmt.executeUpdate();
				rs.close();
				rs = insertStmt.getGeneratedKeys();
				if (rs.next()) {
					nameId = rs.getInt(1);
				} else throw new SQLException("Insertion not passed!");
			}
		} finally {
			if (rs != null) rs.close();
		}
		
		return nameId;
	}

	private void insertIMDb(JsonElement el, int id) throws SQLException {
		if (el != null) {
			JsonObject imdb = el.getAsJsonObject();
			PreparedStatement pr = null;
			try {
				pr = con.prepareStatement(Queries.INSERT_IMDB.getQuery());
				pr.setFloat(1, id);
				pr.setFloat(2, imdb.get("ball").getAsFloat());
				pr.setFloat(3, imdb.get("count").getAsInt());
				pr.executeUpdate();
			} finally {
				if (pr != null) pr.close();
			}
		}
	}
	
	private void insertPersons(JsonObject movie, int id) throws SQLException {
		JsonElement el = movie.get("persons");
		if (el != null) {
			JsonArray persons = el.getAsJsonArray();
			PreparedStatement selectPersonId = null;
			PreparedStatement insertPerson = null;
			PreparedStatement insertStaff = null;
			PreparedStatement selectProfessionId = null;
			PreparedStatement insertProf = null;
			PreparedStatement selectRoleId = null;
			PreparedStatement insertRole = null;
			PreparedStatement insertActorRoles = null;
			try {
				selectPersonId = con.prepareStatement(Queries.SELECT_PERSON_ID.getQuery());
				insertPerson = con.prepareStatement(Queries.INSERT_PERSON.getQuery(), Statement.RETURN_GENERATED_KEYS);
				insertStaff = con.prepareStatement(Queries.INSERT_STAFF.getQuery());
				selectProfessionId = con.prepareStatement(Queries.SELECT_PROFESSION_ID.getQuery());
				insertProf = con.prepareStatement(Queries.INSERT_PROFESSION.getQuery(), Statement.RETURN_GENERATED_KEYS);
				selectRoleId = con.prepareStatement(Queries.SELECT_ROLE_ID.getQuery());
				insertRole = con.prepareStatement(Queries.INSERT_ROLE.getQuery(), Statement.RETURN_GENERATED_KEYS);
				insertActorRoles = con.prepareStatement(Queries.INSERT_ACTORS_ROLES.getQuery());
				
				for (JsonElement person : persons) {
					insertPerson(person, selectPersonId, insertPerson, 
							insertStaff, selectProfessionId, insertProf, 
							selectRoleId, insertRole, insertActorRoles, id);
				}	
			} finally {
				if (insertActorRoles != null) insertActorRoles.close();
				if (insertRole != null) insertRole.close();
				if (selectRoleId != null) selectRoleId.close();
				if (insertProf != null) insertProf.close();
				if (selectProfessionId != null) selectProfessionId.close();
				if (insertStaff != null) insertStaff.close();
				if (insertPerson != null) insertPerson.close();
				if (selectPersonId != null) selectPersonId.close();
			}
		}
	}
	
	private void insertPerson(JsonElement person, PreparedStatement selectPersonId, 
			PreparedStatement insertPerson, PreparedStatement insertStaff, 
			PreparedStatement selectProfessionId, PreparedStatement insertProf, 
			PreparedStatement selectRoleId, PreparedStatement insertRole, 
			PreparedStatement insertActorRoles,int id) throws SQLException {
		
		JsonObject p = person.getAsJsonObject();
		
		// insert into staff
		int personId = getId(p.get("name"), selectPersonId, insertPerson);
		int profId = getId(p.get("profession"), selectProfessionId, insertProf);
		insertStaff.setInt(1, personId);
		insertStaff.setInt(2, id);
		insertStaff.setInt(3, profId);
		insertStaff.executeUpdate();
		
		// insert roles 
		insertRoles(p.get("roles"), selectRoleId, insertRole, insertActorRoles, personId, id, profId);
		
		// clear
		insertProf.clearParameters();
		selectProfessionId.clearParameters();
		insertStaff.clearParameters();
		insertPerson.clearParameters();
		selectPersonId.clearParameters();
	}
	
	private void insertRoles(JsonElement rolesEl, PreparedStatement selectRoleId,
			PreparedStatement insertRole, PreparedStatement insertActorRoles, int personId, int id, int profId) throws SQLException {
		if (rolesEl != null) {
			JsonArray roles = rolesEl.getAsJsonArray();
			for (JsonElement roleEl : roles) {
				// insert role
				insertActorRoles.setInt(1, personId);
				insertActorRoles.setInt(2, id);
				insertActorRoles.setInt(3, profId);
				insertActorRoles.setInt(4, getId(roleEl, selectRoleId, insertRole));
				insertActorRoles.executeUpdate();
				
				insertActorRoles.clearParameters();
			}
			
			insertRole.clearParameters();
			selectRoleId.clearParameters();
		}
	}
	
	public static void main(String[] args) throws IOException {
		QueryExecutor exec = new QueryExecutor();
		try {
			//insert cinemas
			JsonParser parser = new JsonParser();
			JsonElement cinemaJson = parser.parse(FileUtils.readFileToString(new File("/home/shest/cinemas/" + LocalDate.now() + ".json")));
			exec.insertCinemas(cinemaJson.getAsJsonArray());
			
			//insert movies
			JsonElement movieJson = parser.parse(FileUtils.readFileToString(new File("/home/shest/movies/" + LocalDate.now() + ".json")));
			exec.insertCinemas(movieJson.getAsJsonArray());
			
			// insert tickets
			JsonElement json = parser.parse(FileUtils.readFileToString(new File("/home/shest/tickets/"+ LocalDate.now() + ".json")));
			exec.insertTickets(json.getAsJsonArray());
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		}
	}

}
