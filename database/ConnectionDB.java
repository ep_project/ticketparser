package phantomjsparser.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class ConnectionDB {
	
	private static MysqlDataSource mysqlDS;
	
	private ConnectionDB() {
		
	}
		
	public synchronized static Connection getConnection() throws SQLException {
		if (mysqlDS == null) {
			Properties props = new Properties();
			FileInputStream fis = null;
			try {
				fis = new FileInputStream("db.properties");
				props.load(fis);
				mysqlDS = new MysqlDataSource();
				mysqlDS.setURL(props.getProperty("DB_URL"));
				mysqlDS.setUser(props.getProperty("DB_USERNAME"));
				mysqlDS.setPassword(props.getProperty("DB_PASSWORD"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return mysqlDS.getConnection();
	}
	
}
