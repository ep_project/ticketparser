package phantomjsparser.database;

public enum Queries {
	SELECT_ALL_CINEMAS("SELECT * FROM Cinemas"),
	INSERT_CINEMA("INSERT INTO Cinema.Cinemas (name, address) "
			+ "VALUES (?, ?) "
			+ "ON DUPLICATE KEY UPDATE "
			+ "address = VALUES(address)"),
	INSERT_CINEMA_NAME("INSERT INTO Cinema.Cinemas (name) "
			+ "VALUES (?)"),
	INSERT_MOVIE("INSERT INTO Movie.Movies (idMovie, title, original_title, production_year, tagline, release_date, runtime, description) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
			+ "ON DUPLICATE KEY UPDATE "
			+ "title = VALUES(title), "
			+ "original_title = VALUES(original_title), "
			+ "production_year = VALUES(production_year), "
			+ "tagline = VALUES(tagline), "
			+ "release_date = VALUES(release_date), "
			+ "runtime = VALUES(runtime), "
			+ "description = VALUES(description)"),
	INSERT_COUNTRY("INSERT INTO Common.Countries (name) "
			+ "VALUES (?)"),
	INSERT_MOVIES_COUNTRIES("INSERT IGNORE INTO Movie.Countries_Movies (idCountry, idMovie) "
			+ "VALUES (?, ?) "),
	INSERT_GENRE("INSERT INTO Movie.Genres (name) "
			+ "VALUES (?)"),
	INSERT_MOVIES_GENRES("INSERT IGNORE INTO Movie.Genres_Movies (idGenre, idMovie) "
			+ "VALUES (?, ?) "),
	INSERT_KEYWORD("INSERT INTO Common.Keywords (name) "
			+ "VALUES (?)"),
	INSERT_MOVIES_KEYWORDS("INSERT IGNORE INTO Movie.Movie_Keywords (idKeyword, idMovie) "
			+ "VALUES (?, ?) "),
	INSERT_IMDB("INSERT INTO Movie.IMDb (Movies_id, ball, count) "
			+ "values(?, ?, ?) "
			+ "ON DUPLICATE KEY UPDATE "
			+ "ball = values(ball), "
			+ "count = values(count)"),
	INSERT_PERSON("INSERT INTO Common.Persons (name) "
			+ "VALUES (?)"),
	INSERT_STAFF("INSERT IGNORE INTO Movie.Staff (idPerson, idMovie, idProfession) "
			+ "VALUES (?, ?, ?)"),
	INSERT_ROLE("INSERT INTO Movie.Roles (name) "
			+ "VALUES(?)"),
	INSERT_ACTORS_ROLES("INSERT IGNORE INTO Movie.Actor_Roles (idPerson, idMovie, idProfession, idRoles) "
			+ "VALUES(?, ?, ?, ?)"),
	INSERT_TICKET("INSERT INTO Session.Sessions (idCinema, idMovie, price, time, idFormat) "
			+ "VALUES(?, ?, ?, ?, ?)"),
	INSERT_FORMAT("INSERT INTO Session.Formats (name) VALUES(?)"),
	INSERT_PROFESSION("INSERT INTO Common.Professions (name) values(?)"),
	SELECT_COUNTRY_ID("SELECT idCountry from Common.Countries where name = ? Limit 1"),
	SELECT_GENRE_ID("SELECT idGenre from Movie.Genres where name = ? Limit 1"),
	SELECT_KEYWORD_ID("SELECT idKeyword from Common.Keywords where name = ? Limit 1"),
	SELECT_PROFESSION_ID("SELECT idProfession FROM Common.Professions WHERE name = ? Limit 1"),
	SELECT_PERSON_ID("SELECT idPerson FROM Common.Persons WHERE name = ? Limit 1"),
	SELECT_ROLE_ID("SELECT idRoles FROM Movie.Roles WHERE name = ? Limit 1"),
	SELECT_CINEMA_ID("SELECT idCinema FROM Cinema.Cinemas WHERE Cinemas.name =  ? Limit 1"),
	SELECT_FORMAT_ID("SELECT idFormat FROM Session.Formats WHERE Formats.name = ? Limit 1"),
	DELETE_CINEMAS("DELETE FROM Cinema.Cinemas"),
	DELETE_CINEMA("DELETE FROM Cinema.Cinemas WHERE name = ?");
	
	private String query;
	Queries(final String query) {
		this.query = query;
	}
	public String getQuery() {
		return query;
	}
}
