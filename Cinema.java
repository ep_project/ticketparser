package phantomjsparser;

public class Cinema {
	private String name;
	private String address;
	private Float lon;
	private Float lat;
	
	public Cinema() {
	}
	public Cinema(String name) {
		this.name = name;
	}
	
	public Cinema(String name, String address, float lon, float lat) {
		super();
		this.name = name;
		this.address = address;
		this.lon = lon;
		this.lat = lat;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(Float lon) {
		this.lon = lon;
	}
	public Float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
}
