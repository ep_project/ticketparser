package phantomjsparser.parser.kinopoisk;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import phantomjsparser.Country;
import phantomjsparser.Genre;
import phantomjsparser.IMDb;
import phantomjsparser.Keyword;
import phantomjsparser.Movie;
import phantomjsparser.Person;
import phantomjsparser.Profession;
import phantomjsparser.Role;
import phantomjsparser.Staff;
import phantomjsparser.parser.MovieMaker;
import phantomjsparser.parser.PhantomParser;
import phantomjsparser.parser.Urls;


public class KinopoiskMovieParser extends PhantomParser<Movie> implements MovieMaker{
	private Document doc = null;
	private Element table = null;
	private String url = null;
	
	@Override
	public Movie parse(final String url) {
		this.url = url;
		// antiblocking sleep
		try {
			Thread.sleep(50000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		long start = System.nanoTime();
		System.out.println("go to page...");
		driver.get(url);
		System.out.println("go to " + url);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(MovieSelectors.MOVIE_INFO.getSelector()));
		String html = driver.getPageSource();
		doc = Jsoup.parse(html);
		
		Movie movie = makeMovie();
		
		long time = System.nanoTime() - start;
		System.out.println("elapsed time:" + time/1000000 + "s.");
		return movie;
	}
	
	@Override
	public String parseAsJson(String url) {
		Movie movie = parse(url);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(movie);
	}
	
	@Override
	public Movie makeMovie() {
		Movie movie = new Movie();
		movie.setImageRef(parseImageRef());
		movie.setTitle(parseTitle());
		movie.setOriginalTitle(parseOriginalTitle());
		
		table = getTable(MovieSelectors.MOVIE_INFO, doc).get(0);

		movie.setProductionYear(parseProductionYear());
		movie.setCountries(parseCountries());
		movie.setTagline(parseTagline());
		movie.setReleaseDate(parseReleaseDate());
		movie.setRuntime(parseRuntime());
		movie.setImdb(parseImdb());
		movie.setDescription(parseDescription());
		movie.setGenres(parseGenres());
		movie.setKeywords(parseKeywords());
		movie.setStaffs(parseStaffs());
		
		return movie;
	}
	
	public List<String> getPremiersRefs() {
		driver.get(Urls.PREMIERS.toString());
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("premier_item")));

		// scroll down
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		for (int i = 0; i < 4; ++i) {
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		List<String> urls = new ArrayList<String>();
		final String html = driver.getPageSource();
		final Document doc = Jsoup.parse(html);
		Elements premierList = doc.select(MovieSelectors.PREMIERS.asString());
		StringBuilder builder = new StringBuilder();
		for (Element premier : premierList) {
			builder.append("https://www.kinopoisk.ru").append(premier.select("span.name a").attr("href"));
			urls.add(builder.toString());
			builder.setLength(0);
		}
		return urls;
	}
	
	@Override
	public String parseImageRef() {
		return "http://kinopoisk.ru/image/" + getId(url) + ".jpeg";
	}
	
	@Override
	public String parseTitle() {
		return parse(MovieSelectors.KINOPOISK_MOVIE_TITLE, doc);
	}

	@Override
	public String parseOriginalTitle() {
		return parse(MovieSelectors.KINOPOISK_ORIGINAL_MOVIE_TITLE, doc);
	}

	@Override
	public Integer parseProductionYear() {
		String year = parse(table, MovieSelectors.YEAR);
		return Integer.parseInt(year);
		
	}

	@Override
	public String parseTagline() {
		return parse(table, MovieSelectors.TAGLINE);
	}

	@Override
	public LocalDate parseReleaseDate() {
		String res = parse(table, MovieSelectors.KINOPOISK_RELEASE_DATE);
		if (res == null) return null;
		LocalDate date = LocalDate.parse(res.split(", ")[0], DateTimeFormatter.ofPattern("d MMMM yyyy", new Locale("ru")));
		return date;
	}

	@Override
	public Integer parseRuntime() {
		String time = parse(table, MovieSelectors.KINOPOISK_RUNTIME);
		if (time == null) return null;
		assert time.contains(" ");
		return Integer.parseInt(time.substring(0, time.indexOf(" ")));
	}

	@Override
	public String parseDescription() {
		return parse(MovieSelectors.KINOPOISK_DESCRIPTION, doc);
	}

	@Override
	public IMDb parseImdb() {
		Elements rating = doc.select(MovieSelectors.RATING.asString());
		IMDb imdb = null;
		if (!rating.isEmpty()) {
			assert rating.size() == 1;
			Elements imdbEl = rating.get(0).select(MovieSelectors.KINOPOISK_IMDb.asString());
			if (!imdbEl.isEmpty()) {
				assert imdbEl.size() == 1;
				imdb = parseIMDb(imdbEl.get(0).text());
			}
		}
		return imdb;
	}

	@Override
	public Set<Genre> parseGenres() {
		String res = parse(table, MovieSelectors.GENRES);
		Set<Genre> set = new HashSet<>();
		if (res == null) return null;
		String[] elements = res.split(", ");
		for (String el : elements) {
			if (!el.startsWith("...")) {
				set.add(new Genre(el));
			}
		}
		return set;
	}

	@Override
	public Set<Country> parseCountries() {
		String res = parse(table, MovieSelectors.KINOPOISK_COUNTRIES);
		String[] elements = res.split(", ");
		Set<Country> set = new HashSet<>();
		for (String el : elements) {
			if (!el.startsWith("...")) {
				set.add(new Country(el));
			}
		}
		
		return set;
	}

	@Override
	public Set<Keyword> parseKeywords() {
		List<WebElement> keywordButton = driver.findElements(MovieSelectors.OPEN_KEYWORD_BUTTON.getSelector());
		Set<Keyword> set = new HashSet<>();
		if (!keywordButton.isEmpty()) {
			assert keywordButton.size() == 1;
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			keywordButton.get(0).click();
			String html = driver.getPageSource();
			Document doc = Jsoup.parse(html);
			Elements keywords = doc.select(MovieSelectors.KEYWORDS_LIST.asString());
			for (Element list : keywords) {
				Elements words = list.select("li");
				for (Element word : words) {
					set.add(new Keyword(word.text()));
				}
			}
			driver.navigate().back();
		}
		return set;
	}

	@Override
	public Set<Staff> parseStaffs() {
		List<WebElement> personButton = driver.findElements(MovieSelectors.OPEN_ACTORS_BUTTON.getSelector());
		Set<Staff> set = new HashSet<>();
		if (!personButton.isEmpty()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			personButton.get(0).click();
			String html = driver.getPageSource();
			Document actorDoc = Jsoup.parse(html);
			
			// parse all persons
			
			set.addAll(parsePersons(MovieSelectors.KINOPOISK_DIRECTORS, actorDoc, "directors"));
			set.addAll(parsePersons(MovieSelectors.WRITERS, actorDoc, "writers"));
			set.addAll(parsePersons(MovieSelectors.KINOPOISK_ACTORS, actorDoc, "stars"));
			
			driver.navigate().back();

		}
		
		return set;
	}
	
	private Set<Staff> parsePersons(final MovieSelectors selector, final Document doc, String prof) {
		Set<Staff> set = new HashSet<>();
		Elements persons = doc.select(selector.asString());
		if (persons.isEmpty()) return set;
		for (Element person : persons) {
			if (person.select("div[class]").isEmpty()) break;
			set.add(parsePerson(person, prof));
		}
		return set;
	}
	
	private Staff parsePerson(final Element el, String prof) {
		String name = el.select("div.actorInfo div.name > a").text();
		Staff person = new Staff();
		person.setPerson(new Person(name));
		person.setProfession(new Profession(prof));
		person.setRoles(parseRoles(el));
		
		return person;
	}
	
	private Set<Role> parseRoles(final Element el) {
		Set<Role> set = new HashSet<Role>();
		Elements rolesElements = el.select("div.actorInfo div.role");
		if (rolesElements.isEmpty()) return null;
		String[] roles = rolesElements.text()
				.split(", ")[0]
				.replace("...", "")
				.trim()
				.split(" / ");
		for (String role : roles) {
			if (role.isEmpty()) return null;
			set.add(new Role(role));
		}
		return set;
	}
	
	private int getId(final String url) {
		Matcher m = Pattern.compile("[0-9]+").matcher(url);
		int id = 0;
		if (m.find()) {
			id = Integer.parseInt(m.group());
		}
		return id;
	}
	
	private String parse(final MovieSelectors selector, final Document doc) {
		Elements webElement = getElements(selector, doc);
		if (webElement.isEmpty()) return null;	
		String res = webElement.get(0).text();
		
		return res;
	}
	
	private String parse(final Element table, final MovieSelectors selector) {
		Elements elements = table.select(selector.asString());
		if (elements.isEmpty()) return null;
		String res = elements.get(0).text();
		if (res.equals("-")) return null;
		return res;
	}
	
	private IMDb parseIMDb(final String info) {
		IMDb imdb = new IMDb();
		String[] res = info.split("IMDb: |\\s\\(|\\)");
		assert res.length == 3;
		imdb.setBall(Float.parseFloat(res[1]));
		imdb.setCount(Integer.parseInt(res[2].replaceAll(" ", "")));
		return imdb;
	}
	
	private Elements getTable(final MovieSelectors selector, final Document doc) {
		Elements webElements = getElements(selector, doc);
		assert webElements.size() >= 1;
		
		return webElements;
	}
	
	private Elements getElements(final MovieSelectors selector, final Document doc) { 
		return doc.select(selector.asString());
	}

	public static void main(String[] args) throws InterruptedException, IOException {
		KinopoiskMovieParser parser = new KinopoiskMovieParser();
		try {
			List<String> urls = parser.getPremiersRefs();
			System.out.println(urls.size());
			for (String ref : urls) {
				String movie = parser.parseAsJson(ref);
				System.out.println(movie);
			}	
		} finally {
			parser.close();
		}
	}
}
