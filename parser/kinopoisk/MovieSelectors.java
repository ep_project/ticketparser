package phantomjsparser.parser.kinopoisk;

import org.openqa.selenium.By;

enum MovieSelectors {
	KINOPOISK_MOVIE_TITLE(Types.CSS, "div#headerFilm > h1"),
	KINOPOISK_ORIGINAL_MOVIE_TITLE(Types.CSS, "div#headerFilm > span"),
	MOVIE_INFO(Types.CSS, "table.info"),
	RATING(Types.CSS, "form.rating_stars"),
	KINOPOISK_DESCRIPTION(Types.CSS, "div[itemprop='description']"),
	OPEN_ACTORS_BUTTON(Types.CSS, "div#actorList > h4 > a"),
	KEYWORDS_LIST(Types.CSS, "ul.keywordsList"),
	KINOPOISK_ACTORS(Types.CSS, "a[name='actor'] + div ~ div"),
	WRITERS(Types.CSS, "a[name='writer'] + div ~ div"),
	KINOPOISK_DIRECTORS(Types.CSS, "a[name='director'] + div ~ div"),
	PREMIERS(Types.CSS, "div.prem_list > div.premier_item"),
	TAGLINE(Types.CSS, "td:contains(слоган) +td"),
	GENRES(Types.CSS, "td:contains(жанр) + td"),
	KINOPOISK_RELEASE_DATE(Types.CSS, "td:contains(премьера (РФ)) + td"),
	KINOPOISK_RUNTIME(Types.CSS, "td:contains(время) + td"),
	KINOPOISK_IMDb(Types.CSS, "div:contains(IMDb)"),
	KINOPOISK_COUNTRIES(Types.CSS, "td:contains(страна) + td"),
	YEAR(Types.CSS, "td:contains(год) + td"),
	OPEN_KEYWORD_BUTTON(Types.XPATH, "//*[contains(text(), 'жанр')]/../td[2]//a[@class='wordLinks']");

	private String query;
	private Types type;

	MovieSelectors(Types type, String query) {
		this.query = query;
		this.type = type;
	}
	
	private enum Types {
		CSS,
		XPATH;
	}
	
	String asString() {
		return query;
	}

	By getSelector() {
		switch (type) {
		case CSS:
			return By.cssSelector(query);
		case XPATH:
		default:
			return By.xpath(query);
		}
	}
}
