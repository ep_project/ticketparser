package phantomjsparser.parser;

import java.time.LocalDate;
import java.util.Set;

import phantomjsparser.Country;
import phantomjsparser.Genre;
import phantomjsparser.IMDb;
import phantomjsparser.Keyword;
import phantomjsparser.Movie;
import phantomjsparser.Staff;

public interface MovieMaker {
	 String parseTitle();
	 String parseOriginalTitle();
	 Integer parseProductionYear();
	 String parseTagline();
	 LocalDate parseReleaseDate();
	 Integer parseRuntime();
	 String parseDescription();
	 IMDb parseImdb();
	 String parseImageRef();
	 Set<Genre> parseGenres();
	 Set<Country> parseCountries();
	 Set<Keyword> parseKeywords();
	 Set<Staff> parseStaffs();
	 
	 Movie makeMovie();
}
