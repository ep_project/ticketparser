package phantomjsparser.parser;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class PhantomParser<T> implements Parser<T> {
	private final String phantombinary = "/home/shest/eclipse/phantomjs-2.1.1-linux-x86_64/bin/phantomjs";
	protected WebDriver driver = setDriver();
	protected WebDriverWait wait = new WebDriverWait(driver, 30);
	
		
	public void close() {
		driver.quit();
	};
	
	protected WebDriver setDriver() {
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setJavascriptEnabled(true);
		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantombinary);
		WebDriver driver = new PhantomJSDriver(caps);
		driver.manage().window().setSize(new Dimension(1280, 1024));
		return driver;
	}
	
}
