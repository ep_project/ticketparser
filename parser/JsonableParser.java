package phantomjsparser.parser;

public interface JsonableParser {
	String parseAsJson(String url);
}
