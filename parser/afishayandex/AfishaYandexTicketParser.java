package phantomjsparser.parser.afishayandex;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import phantomjsparser.Cinema;
import phantomjsparser.Format;
import phantomjsparser.Movie;
import phantomjsparser.Session;
import phantomjsparser.parser.JsonableParser;
import phantomjsparser.parser.Urls;

public class AfishaYandexTicketParser extends AfishaYandexEventParser<List<Session>> implements JsonableParser {	
	private WebDriver ticketFrame = setDriver();
	private WebDriverWait ticketWait = new WebDriverWait(ticketFrame, 30);

	@Override
	public List<Session> parse(final String url) {
		List<WebElement> elements = getMovieElements(url);
		List<Session> sessions = new ArrayList<>();
		// parse for tickets
		for (WebElement el : elements) {
			if (!hasTickets(el)) continue;

			waitAndClick(el.findElement(By.cssSelector("a")));
			sessions.addAll(getTickets());
		}
		
		return sessions;
	}
	
	@Override
	public String parseAsJson(String url) {
		List<Session> sessions = parse(url);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(sessions);
	}
	
	@Override
	public void close() {
		driver.quit();
		ticketFrame.quit();
	}


	private boolean hasTickets(WebElement el) {
		List<WebElement> hasTickets = el.findElements(SessionSelectors.AVAILABLE_TICKETS.getSelector());
		assert hasTickets.size() <= 1;
		return hasTickets.isEmpty() ? false : true;
	}

	private List<Session> getTickets() {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(SessionSelectors.POPUP_HEADING.getSelector()));

		List<WebElement> btn = getSessionButtons();
		if (btn.isEmpty()) {
			closePopup();
			return new ArrayList<>();
		}
				
		Movie movie = parseMovie();
		
		// go to movie information frame
		gotoTicketFrame(btn.get(0));
		
		// select all movie theaters for the movie
		List<WebElement> cinemas = getSessionItems();

		assert !cinemas.isEmpty();
		
		return parseTickets(movie, cinemas);
		
	}
	
	private List<Session> parseTickets(final Movie movie, final List<WebElement> cinemas) {
		List<Session> sessions = new ArrayList<Session>();

		for (WebElement cinemaEl : cinemas) {
			// make Movie theater
			Cinema cinema = parseCinema(cinemaEl);
			
			// select all formats for the movie theater
			List<WebElement> ticketItems = cinemaEl.findElements(SessionSelectors.TICKETS.getSelector());
			assert !ticketItems.isEmpty();

			for (WebElement ticketItem : ticketItems) {
				
				Format format = parseFormat(ticketItem);
				List<WebElement> times = ticketItem.findElements(SessionSelectors.TICKET_TIME.getSelector());
				assert !times.isEmpty();

				for (WebElement timeButton : times) {
					
					LocalTime time = parseTime(timeButton);
					List<Integer> prices = parsePrices(timeButton);
					for (int price : prices) {
						Session session = new Session();
						session.setMovie(movie);
						session.setCinema(cinema);
						session.setFormat(format);
						session.setPrice(price);
						session.setTime(time);
						sessions.add(session);
					}
				}
			}
		}
		
		return sessions;
	}

	private Cinema parseCinema(WebElement cinemaEl) {
		return new Cinema(cinemaEl.findElement(SessionSelectors.CINEMA_NAME.getSelector())
				.getAttribute("innerText"));
	}

	
	private Movie parseMovie() {
		String title = getTitle();
		int year = getProductionYear();
		
		return new Movie(title, year);
	}

	
	private List<Integer> parsePrices(WebElement timeButton) {
		String pricesStr = timeButton.getAttribute("data-prices");
		List<Integer> res = new ArrayList<>();
		if (pricesStr == null) return res;
		String[] prices = pricesStr.split(",");
		for (String price : prices) {
			res.add(Integer.parseInt(price));
		}
		return res;
	}

	
	private LocalTime parseTime(WebElement timeButton) {
		String time = timeButton.getAttribute("innerText");
		return LocalTime.parse(time);
	}

	
	private Format parseFormat(WebElement el) {
		List<WebElement> formats = el.findElements(SessionSelectors.TICKET_FORMAT.getSelector());
		assert formats.size() == 1;
		return new Format(formats.get(0).getAttribute("innerText"));
	}
	
	private void closePopup() {
		wait.until(ExpectedConditions.presenceOfElementLocated(SessionSelectors.POPUP_CLOSER.getSelector()));
		List<WebElement> closeBtn = driver.findElements(SessionSelectors.POPUP_CLOSER.getSelector());
		assert closeBtn.size() == 1;
		waitAndClick(closeBtn.get(0));
	}
	
	private void gotoTicketFrame(WebElement button) {
		waitAndClick(button.findElement(By.cssSelector("li")));
		String id = button.getAttribute("data-session-id");
		List<WebElement> frames = driver.findElements(By.tagName("iframe"));
		for (WebElement frame : frames) {
			if (frame.getAttribute("outerHTML").contains("src=\"https://widget.tickets.yandex.ru/w/sessions/" + id)) {
				ticketFrame.get(frame.getAttribute("src"));
				break;
			}
		}
		
		ticketWait.until(
				ExpectedConditions.presenceOfAllElementsLocatedBy(SessionSelectors.TICKET_FRAME_BACK_BUTTON.getSelector()));
		
		List<WebElement> backButton = ticketFrame.findElements(SessionSelectors.TICKET_FRAME_BACK_BUTTON.getSelector());
		assert backButton.size() == 1;
		waitAndClick(backButton.get(0));
	}

	private void takeScreenshot(String name) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File("/home/shest/eclipse/" + name + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<WebElement> getSessionButtons() {
		return driver.findElements(SessionSelectors.TICKET_FRAME_BUTTON.getSelector());
	}

	private List<WebElement> getSessionItems() {
		ticketWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(SessionSelectors.SESSIONS.getSelector()));
		return ticketFrame.findElements(SessionSelectors.SESSIONS.getSelector());
	}

	public static void main(String[] args) {
		AfishaYandexTicketParser parser = new AfishaYandexTicketParser();
		try {
			String tickets = parser.parseAsJson(Urls.TICKETS_SPB_TODAY.getUrl());
			System.out.println(tickets);
			File jsonFile = new File("/home/shest/tickets/" + LocalDate.now() + ".json");
			FileWriter	writer = new FileWriter(jsonFile);
			writer.write(tickets);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			parser.close();
		}
	}

	

	


}
