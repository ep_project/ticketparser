package phantomjsparser.parser.afishayandex;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import phantomjsparser.Cinema;
import phantomjsparser.parser.JsonableParser;
import phantomjsparser.parser.PhantomParser;
import phantomjsparser.parser.Urls;

public class AfishaYandexCinemaParser extends PhantomParser<List<Cinema>> implements JsonableParser {
	
	@Override
	public List<Cinema> parse(String url) {
		List<WebElement> cinemaElements = getCinemaElements(url);
		List<Cinema> cinemas = new ArrayList<>();
		for (WebElement cinemaElement : cinemaElements) {
			Cinema cinema = new Cinema();
			cinema.setName(parse(CinemaSelectors.CINEMA_TITLE, cinemaElement));
			cinema.setAddress(parse(CinemaSelectors.CINEMA_ADRESS, cinemaElement));
			cinemas.add(cinema);
		}
		
		return cinemas;
	}
	
	@Override
	public String parseAsJson(String url) {
		List<Cinema> cinemas = parse(url);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(cinemas);
	}
	
	private List<WebElement> getCinemaElements(String url) {
		driver.get(url);
		openAllCinemas();
		return driver.findElements(CinemaSelectors.CINEMA_ELEMENTS.getSelector());
	}

	private void openAllCinemas() {
		while (hasMoreButton()) {
			driver.findElement(CinemaSelectors.MORE_CINEMA_BUTTON.getSelector()).click();
		}
	}
	
	private boolean hasMoreButton() {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(CinemaSelectors.MORE_CINEMA_BUTTON.getSelector()));
		List<WebElement> buttons = driver.findElements(CinemaSelectors.MORE_CINEMA_BUTTON.getSelector());
		assert buttons.size() == 1;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buttons.get(0).isDisplayed();
	}
	
	private String parse(CinemaSelectors selector, WebElement el) {
		return el.findElement(selector.getSelector()).getAttribute("innerText");
	}

	public static void main(String[] args) {
		AfishaYandexCinemaParser parser = new AfishaYandexCinemaParser();
		try {
			String json = parser.parseAsJson(Urls.CINEMAS_SPB.getUrl());
			System.out.println(json);
			File jsonFile = new File("/home/shest/cinemas/" + LocalDate.now() + ".json");
			FileWriter	writer = new FileWriter(jsonFile);
			writer.write(json);
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			parser.close();
		}

	}

	
}
