package phantomjsparser.parser.afishayandex;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import phantomjsparser.parser.PhantomParser;

public abstract class AfishaYandexEventParser<T> extends PhantomParser<T> {
		
	protected List<WebElement> getMovieElements(String url) {
		driver.get(url);
		openAllMovies();
		return driver.findElements(SessionSelectors.MOVIE_ELEMENTS.getSelector());
	}
	
	protected void waitAndClick(WebElement el) {
		while (true) {
			try {
				el.click();
				return;
			} catch (Exception ex) {
			}
		}
	}

	protected String getTitle() {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(SessionSelectors.POPUP_HEADING.getSelector()));
		WebElement titleEl = driver.findElement(SessionSelectors.MOVIE_TITLE.getSelector());
		return titleEl.getAttribute("innerText");
	}
	
	protected int getProductionYear() {
		WebElement yearEl= driver.findElement(SessionSelectors.PRODUCTION_YEAR.getSelector());
		return Integer.parseInt(yearEl.getAttribute("innerText"));
	}
	
	private void openAllMovies() {

		while (hasMoreButton()) {
			WebElement b = getMoreButton();
			b.click();
		}
	}
	
	private boolean hasMoreButton() {
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(SessionSelectors.MORE_MOVIE_BUTTON.getSelector()));
		List<WebElement> buttons = driver.findElements(SessionSelectors.MORE_MOVIE_BUTTON.getSelector());
		assert buttons.size() == 1;
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return buttons.get(0).isDisplayed();
	}
	
	private WebElement getMoreButton() {
		return driver.findElement(SessionSelectors.MORE_MOVIE_BUTTON.getSelector());
	}
	
	
}
