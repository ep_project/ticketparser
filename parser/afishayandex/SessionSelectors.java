package phantomjsparser.parser.afishayandex;


import org.openqa.selenium.By;

enum SessionSelectors {
	TICKET_FRAME_BUTTON(Types.CSS, "span.js-yaticket-button[data-session-id]"),
	TICKET_FRAME_BACK_BUTTON(Types.CSS, "div.hallplan-footer_back b"),
	MOVIE_ELEMENTS(Types.CSS, "div[class^='event'][itemtype='https://schema.org/Event']"),
	MORE_MOVIE_BUTTON(Types.CSS, "div.events-list__more > a[class^='button']"),
	MOVIE_TITLE(Types.CSS, "h1.event-heading__title"),
	GENRE(Types.CSS, "div.event__formats"),
	SHORT_REVIEW(Types.CSS, "div.event__argument"),
	RATE(Types.CSS, "div.event-rating__value"),
	AVAILABLE_TICKETS(Types.CSS, "div.event__tickets"),
	CINEMA_NAME(Types.CSS, "div.session-item-caption > span.session-item-caption_title"),
	FRAME_CLOSER(Types.CSS, "div.container-slide_controls > b.frame-closer"),
	POPUP_CLOSER(Types.CSS, "div[class^='popup'] i.popup__close"),
	POPUP_HEADING(Types.CSS, "div.event-heading"),
	KINOPOISK_MOVIE_ID(Types.CSS, "div.smart-grid__main dl.event-attributes__inner > a"),
	SESSIONS(Types.CSS, "div.session-item"),
	TICKETS(Types.CSS, "li.session-item-list_group"),
	TICKET_FORMAT(Types.CSS, "span[class$='format-inner'] span[data-reactid$='0']"),
	TICKET_TIME(Types.CSS, "b.button"),
	DESCRIPTION(Types.CSS, "div[itemprop='description']"),
	IMDb(Types.CSS, "div.arrow__text"),
	RUNTIME(Types.XPATH, "//*[contains(text(), 'Время')]/../dd"),
	PRODUCTION_YEAR(Types.XPATH, "//*[contains(text(), 'Год производства')]/../dd"),
	ORIGINAL_TITLE(Types.XPATH, "//*[contains(text(), 'Оригинальное название')]/../dd"),
	RELEASE_DATE(Types.XPATH, "//*[contains(text(), 'Премьера')]/../dd"),
	COUNTRIES(Types.XPATH, "//*[contains(text(), 'Страна')]/../dd"),
	ACTORS(Types.XPATH, "//*[contains(text(), 'В ролях')]/../dd"), 
	DIRECTORS(Types.XPATH, "//*[contains(text(), 'Режиссёр')]/../dd");
	
	private String query;
	private Types type;

	SessionSelectors(Types type, String query) {
		this.query = query;
		this.type = type;
	}
	
	private enum Types {
		CSS,
		XPATH;
	}
	
	public String asString() {
		return query;
	}

	public By getSelector() {
		switch (type) {
		case CSS:
			return By.cssSelector(query);
		case XPATH:
		default:
			return By.xpath(query);
		}
	}
}

