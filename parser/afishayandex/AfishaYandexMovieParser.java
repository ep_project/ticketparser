package phantomjsparser.parser.afishayandex;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import phantomjsparser.Country;
import phantomjsparser.Genre;
import phantomjsparser.IMDb;
import phantomjsparser.Keyword;
import phantomjsparser.Movie;
import phantomjsparser.Person;
import phantomjsparser.Profession;
import phantomjsparser.Staff;
import phantomjsparser.parser.MovieMaker;
import phantomjsparser.parser.Urls;
import phantomjsparser.parser.kinopoisk.KinopoiskMovieParser;

public class AfishaYandexMovieParser extends AfishaYandexEventParser<List<Movie>> implements MovieMaker
{	
	@Override
	public List<Movie> parse(String url) {
		List<WebElement> elements = getMovieElements(url);
		List<Movie> movies = new ArrayList<Movie>();
		for (WebElement el : elements) {
			waitAndClick(el.findElement(By.cssSelector("a")));
			movies.add(makeMovie());
		}
		
		return movies;
	}
	
	@Override
	public String parseAsJson(String url) {
		List<Movie> movies = parse(url);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(movies);
	}
	
	@Override
	public Movie makeMovie() {
		List<WebElement> el = driver.findElements(SessionSelectors.KINOPOISK_MOVIE_ID.getSelector());
		if (!el.isEmpty()) {
			String href = el.get(0).getAttribute("href");
			KinopoiskMovieParser p = new KinopoiskMovieParser();
			try {
				return p.parse(href);
			} finally {
				p.close();
			}
		}
		
		Movie movie = new Movie();
		
		movie.setTitle(parseTitle());
		movie.setOriginalTitle(parseOriginalTitle());
		movie.setProductionYear(parseProductionYear());
		movie.setRuntime(parseRuntime());
		movie.setDescription(parseDescription());
		movie.setImdb(parseImdb());
		movie.setCountries(parseCountries());
		movie.setReleaseDate(parseReleaseDate());
		movie.setStaffs(parseStaffs());
		
		return movie;
	}

	@Override
	public String parseImageRef() {
		// TODO parse image
		return null;
	}
	
	@Override
	public String parseTitle() {
		return getTitle();
	}

	@Override
	public String parseOriginalTitle() {
		List<WebElement> el = driver.findElements(SessionSelectors.ORIGINAL_TITLE.getSelector());
		if (el.isEmpty()) return null;
		return el.get(0).getAttribute("innerText");
	}

	@Override
	public Integer parseProductionYear() {
		return getProductionYear();
	}

	@Override
	public String parseTagline() {
		//no tagline
		return null;
	}

	@Override
	public LocalDate parseReleaseDate() {
		List<WebElement> el = driver.findElements(SessionSelectors.RELEASE_DATE.getSelector());
		if (el.isEmpty()) return null;
		LocalDate date = LocalDate.parse(el.get(0).getAttribute("innerText"), DateTimeFormatter.ofPattern("d MMMM yyyy", new Locale("ru")));
		return date;
	}

	@Override
	public Integer parseRuntime() {
		List<WebElement> el = driver.findElements(SessionSelectors.RUNTIME.getSelector());
		if (el.isEmpty()) return null;
		return Integer.parseInt(el.get(0).getAttribute("innerText").split(" ")[0]);
	}

	@Override
	public String parseDescription() {
		List<WebElement> el = driver.findElements(SessionSelectors.DESCRIPTION.getSelector());
		if (el.isEmpty()) return null;
		return el.get(0).getAttribute("innerText");
	}

	@Override
	public IMDb parseImdb() {
		List<WebElement> el = driver.findElements(SessionSelectors.IMDb.getSelector());
		if (el.isEmpty()) return null;
		return new IMDb(Float.parseFloat(el.get(0).getAttribute("innerText")));
	}

	@Override
	public Set<Genre> parseGenres() {
		// no genres
		return null;
	}

	@Override
	public Set<Country> parseCountries() {
		List<WebElement> el = driver.findElements(SessionSelectors.COUNTRIES.getSelector());
		if (el.isEmpty()) return null;
		Set<Country> set = new HashSet<Country>();
		String [] countries = el.get(0).getAttribute("innerText").split(" ");
		for (String country : countries) {
			set.add(new Country(country));
		}
		return set;
	}

	@Override
	public Set<Keyword> parseKeywords() {
		// no keywords
		return null;
	}

	@Override
	public Set<Staff> parseStaffs() {
		List<WebElement> el = driver.findElements(SessionSelectors.DIRECTORS.getSelector());
		Set<Staff> set = new HashSet<Staff>();
		if (!el.isEmpty()) {
			String [] directors = el.get(0).getAttribute("innerText").split(",");
			for (String director : directors) {
				set.add(new Staff(new Person(director), new Profession("director")));
			}
		}
		el = driver.findElements(SessionSelectors.ACTORS.getSelector());
		if (!el.isEmpty()) {
			String [] actors = el.get(0).getAttribute("innerText").split(",");
			for (String actor : actors) {
				set.add(new Staff(new Person(actor), new Profession("star")));
			}
		}
		return set;
	}
	
	
	
	public static void main(String[] args) {
		AfishaYandexMovieParser parser = new AfishaYandexMovieParser();
		try {
			String movies = parser.parseAsJson(Urls.TICKETS_SPB_TODAY.getUrl());
			System.out.println(movies);
			File jsonFile = new File("/home/shest/movies/" + LocalDate.now() + ".json");
			FileWriter	writer = new FileWriter(jsonFile);
			writer.write(movies);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			parser.close();
		}
	}

}
