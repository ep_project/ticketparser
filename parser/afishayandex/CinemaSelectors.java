package phantomjsparser.parser.afishayandex;

import org.openqa.selenium.By;

enum CinemaSelectors {
		CINEMA_ELEMENTS(Types.CSS, "div[class$='places-list__item']"),
		MORE_CINEMA_BUTTON(Types.CSS, "div.places-list__more > a[class^='button']"),
		CINEMA_TITLE(Types.CSS, "h3.place__title"),
		CINEMA_ADRESS(Types.CSS, "div.place__address");
		
		private String query;
		private Types type;

		CinemaSelectors(Types type, String query) {
			this.query = query;
			this.type = type;
		}
		
		private enum Types {
			CSS,
			XPATH;
		}
		
		public String asString() {
			return query;
		}

		public By getSelector() {
			switch (type) {
			case CSS:
				return By.cssSelector(query);
			case XPATH:
			default:
				return By.xpath(query);
			}
		}
}
