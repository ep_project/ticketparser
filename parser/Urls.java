package phantomjsparser.parser;

public enum Urls {
	PREMIERS("https://www.kinopoisk.ru/premiere/ru/"),
	CINEMAS_SPB("https://afisha.yandex.ru/places?city=saint-petersburg&tag=cinema_theater"),
	TICKETS_SPB_TODAY("https://afisha.yandex.ru/events?city=saint-petersburg&tag=cinema&preset=today");

	private final String url;
	
	Urls(final String url) {
		this.url = url;
	}
	
	public String getUrl() {
		return url;
	}
}
