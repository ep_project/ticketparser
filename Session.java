package phantomjsparser;

import java.time.LocalTime;

public class Session {
	private Cinema cinema;
	private Movie movie;
	private int price;
	private LocalTime time;
	private Format format;
	
	public Session() {
	}
	
	public Session(Cinema cinema, Movie movie, int price, LocalTime time, Format format) {
		this.cinema = cinema;
		this.movie = movie;
		this.price = price;
		this.time = time;
		this.format = format;
	}
	
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public Movie getMovie() {
		return movie;
	}
	public void setMovie(Movie movie) {
		this.movie = movie;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public LocalTime getTime() {
		return time;
	}
	public void setTime(LocalTime time) {
		this.time = time;
	}
	public Format getFormat() {
		return format;
	}
	public void setFormat(Format format) {
		this.format = format;
	}
}
