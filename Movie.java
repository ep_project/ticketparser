    package phantomjsparser;

import java.time.LocalDate;
import java.util.Set;

public class Movie {
	
	private String imageRef;
	
	private String title;
	private String originalTitle;
	private int productionYear;
	private String tagline;
	private LocalDate releaseDate;
	private Integer runtime;
	private String description;
	private IMDb imdb;
	private Set<Genre> genres;
	private Set<Country> countries;
	private Set<Keyword> keywords;
	private Set<Staff> staffs;
	
	public Movie() {
	}
	
	public Movie(String title, int productionYear) {
		this.title = title;
		this.productionYear = productionYear;
	}
	
	public Movie(String title, String originalTitle, int productionYear, String tagline, LocalDate releaseDate, int runtime,
			String description, IMDb imdb, Set<Genre> genres, Set<Country> countries, Set<Keyword> keywords,
			Set<Staff> staffs) {
		this.title = title;
		this.originalTitle = originalTitle;
		this.productionYear = productionYear;
		this.tagline = tagline;
		this.releaseDate = releaseDate;
		this.runtime = runtime;
		this.description = description;
		this.imdb = imdb;
		this.genres = genres;
		this.countries = countries;
		this.keywords = keywords;
		this.staffs = staffs;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOriginalTitle() {
		return originalTitle;
	}
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	public int getProductionYear() {
		return productionYear;
	}
	public void setProductionYear(int productionYear) {
		this.productionYear = productionYear;
	}
	public String getTagline() {
		return tagline;
	}
	public void setTagline(String tagline) {
		this.tagline = tagline;
	}
	public LocalDate getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(LocalDate releaseDate) {
		this.releaseDate = releaseDate;
	}
	public Integer getRuntime() {
		return runtime;
	}
	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public IMDb getImdb() {
		return imdb;
	}
	public void setImdb(IMDb imdb) {
		this.imdb = imdb;
	}
	public Set<Genre> getGenres() {
		return genres;
	}
	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}
	public Set<Country> getCountries() {
		return countries;
	}
	public void setCountries(Set<Country> countries) {
		this.countries = countries;
	}
	public Set<Keyword> getKeywords() {
		return keywords;
	}
	public void setKeywords(Set<Keyword> keywords) {
		this.keywords = keywords;
	}
	public Set<Staff> getStaffs() {
		return staffs;
	}
	public void setStaffs(Set<Staff> staffs) {
		this.staffs = staffs;
	}
	
	public String getImageRef() {
		return imageRef;
	}

	public void setImageRef(String imageRef) {
		this.imageRef = imageRef;
	}
	
}
