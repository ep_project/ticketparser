package phantomjsparser;

public class IMDb {
	private Float ball;
	private Integer count;
	
	public IMDb() {
	}
	
	public IMDb(Float ball) {
		this.ball = ball;
	}
	
	public IMDb(Float ball, Integer count) {
		this.ball = ball;
		this.count = count;
	}
	
	public Float getBall() {
		return ball;
	}
	public void setBall(Float ball) {
		this.ball = ball;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
