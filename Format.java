package phantomjsparser;

public class Format {
	private String name;
	public Format() {
	}
	
	public Format(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
