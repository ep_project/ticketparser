package phantomjsparser;

public class Profession {
	private String name;
	
	public Profession() {
	}
	
	public Profession(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
